package GCI;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

import pageObjects.Homepage;
import resources.base;

public class HomepageTest extends base{
	
	@Test
	public void hmpage() throws IOException, InterruptedException
	{
		driver = initializeDriver();
		driver.get("https://giftcardsindia.com/");
	
		
		Homepage hp=new Homepage(driver);
		//hp.clicksignin().click();
		
		//Click the OK button on the accept cookie
				Thread.sleep(2000);
				//driver.findElement(By.cssSelector("div. ")).click();
				driver.findElement(By.cssSelector("div.cookie-modal a.cookie-close")).click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",
			            hp.clicksignin());
	}

}
